//
//  testproductApp.swift
//  Shared
//
//  Created by Macbook Pro BTS.id on 16/03/22.
//

import SwiftUI

@main
struct testproductApp: App {
    var body: some Scene {
        WindowGroup {
            RestaurantListView()
        }
    }
}
