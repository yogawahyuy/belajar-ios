//
//  ContentView.swift
//  Shared
//
//  Created by Macbook Pro BTS.id on 16/03/22.
//

import SwiftUI
//var restaurantName=["jayeng","Seruni","Moe","Nasi Padang","padang salero","nasi goreng"]
//var restautantImage=["cafedeadend","graham","haigh","homei","palomino","restaurant"]
//var restaurantType=["coffe","tea","water","sushi","sasimi","fried"]
//var restaurantLocation=["japan","indonesia","singapur","malaysia","brunei","china"]
struct RestaurantListView: View {
    @State var restaurant = [Restaurant(name: "jayeng", type: "coffe", location: "japan", image: "cafedeadend", isFavorite: false),
                             Restaurant(name: "Seruni", type: "water", location: "indonesia", image: "graham", isFavorite: false),
                             Restaurant(name: "Moe", type: "water", location: "singapur", image: "haigh", isFavorite: false),
                             Restaurant(name: "Nasi Padang", type: "sushi", location: "malaysia", image: "homei", isFavorite: false),
                             Restaurant(name: "padang salero", type: "sasimi", location: "brunei", image: "palomino", isFavorite: false),
                             Restaurant(name: "nasi goreng", type: "fried", location: "china", image: "restaurant", isFavorite: false)
    ]

    var body: some View {
        List{
            ForEach(restaurant.indices, id: \.self){ index in
                BasicTextImageRow(restaurant: $restaurant[index])
                    .swipeActions(edge: .leading, allowsFullSwipe: false, content:{
                        
                        Button (action: {}, label: {Image(systemName: "heart")}).tint(.orange)
                        
                        Button (action: {}, label: {Image(systemName: "square.and.arrow.up")}).tint(.green)
                        
                        
                    })
            }
            .onDelete(perform: { IndexSet in restaurant.remove(atOffsets: IndexSet)})
            .listRowSeparator(.hidden)
        }.listStyle(.plain)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        RestaurantListView()
        RestaurantListView().preferredColorScheme(.dark)
    }
}

struct BasicTextImageRow : View{
    @State private var showOptions = false
    @State private var showError=false
    @Binding var restaurant : Restaurant
    var body : some View{
        HStack(alignment: .top, spacing: 20){
            Image(restaurant.image)
                .resizable()
                .frame(width: 120, height: 118)
                .cornerRadius(20)
            
            VStack(alignment: .leading){
                Text(restaurant.name).font(.system(.title,design: .rounded))
                Text(restaurant.type).font(.system(.body, design: .rounded))
                Text(restaurant.location).font(.system(.subheadline,design:.rounded))
                    .foregroundColor(.gray)
            }
            if restaurant.isFavorite{
                Spacer()
                Image(systemName: "heart.fill")
                    .foregroundColor(.red)
            }
            
        }
        .onTapGesture {
            showOptions.toggle()
        }
        .actionSheet(isPresented: $showOptions){
            ActionSheet(title: Text("What do you want todo?"),
            message: nil,
            buttons: [
                .default(Text("Resevere Table")){
                    self.showError.toggle()
                },
                .default(Text(restaurant.isFavorite ? "Remove Favorite":"Mark As Favorite")){
                    self.restaurant.isFavorite.toggle()
                },
                .cancel()
            ])
        }
        .alert(isPresented: $showError){
            Alert(title: Text("Not Yet Available"),
                  message: Text("Sorry, this feature is unavalable"),
                  primaryButton: .default(Text("OK")),
                  secondaryButton: .cancel())
        }
        
    }
}

struct FullImageRow : View{
    var imageName:String
    var name:String
    var type: String
    var location : String
    
    var body: some View{
        VStack(alignment: .leading, spacing: 10){
            Image(imageName)
                .resizable()
                .scaledToFill()
                .frame(height: 200)
                .cornerRadius(20)
            
            VStack(alignment: .leading){
                Text(name).font(.system(.title,design: .rounded))
                Text(type).font(.system(.body, design: .rounded))
                Text(location).font(.system(.subheadline,design:.rounded))
                    .foregroundColor(.gray)
            }
            .padding(.horizontal)
            .padding(.bottom)
        }
    }
    
}
